use std::{
    path::{
        Path,
        PathBuf
    },
    env
};

fn get_root() -> Option<PathBuf> {
    let mut path = env::current_dir().unwrap();
    'traverse_up: loop {
        if Path::new(".ct.lua").is_file() {
            // check if root = true
            // if root {
            //     break 'traverse_up
            // }
        }
        if path == Path::new("/") {
            return None;
        }
        path.pop();
        env::set_current_dir(&path).unwrap();
    }
}

fn main() {
    println!("Hello, world!");
}
